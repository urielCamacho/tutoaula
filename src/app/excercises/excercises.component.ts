import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Http, Response } from '@angular/http';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-excercises',
  templateUrl: './excercises.component.html',
  styleUrls: ['./excercises.component.css']
})
export class ExcercisesComponent implements OnInit {
  rol = 0;
  resInput = 0;
  constructor(private authService : AuthService, private router: Router, public snackBar: MatSnackBar) { }

  ngOnInit() {
    
  }
  loadTangram(){
    return "../../assets/images/gallo.png";
  }
  onSubmit1(form : NgForm){
    if(form.form.controls.valuequestion.value==71775 && this.rol==3){
      this.snackBar.open('Correcto', 'cerrar', {
        duration: 2000,
      });
    }else{
      this.snackBar.open('Incorrecto', 'cerrar', {
        duration: 2000,
      });
      alert(form.form.controls.valuequestion.value+', '+this.rol);
    }
  } 
}
