import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  username : string = '';
  password : string = '';
  password2 : string = '';
  email : string = '';
  rol : string = '';
  constructor(private authService : AuthService) { }

  ngOnInit() {
  }
  loadLogo(){
    return "../../assets/images/logo.png";
  }
  loadImage(){
    return "../../assets/images/CABjaguarazul.png";
  }
  resetForm(form: NgForm){
    if(form.form.controls.password !== form.form.controls.password2){
      alert('Contraseñas erróneas');      
    }else{
      form.reset('');
    }
  }
  onSubmit(form : NgForm){
    this.username = form.form.controls.username.value;
    this.password = form.form.controls.password.value;
    this.password2 = form.form.controls.password2.value;
    this.email = form.form.controls.email.value;
    (this.password === this.password2) 
      && this.password!='' ? this.register(this.username,this.password,this.email) : this.resetForm(form);
    
  }
  register(username : string, pass : string, email : string){
    console.log('holi :v'+
      username+','+pass+','+email+','+this.rol
    );/*
    this.authService.register(user,email,pass, rol)
    .subscribe(
      (response) => {
        console.log(response);

        alert(response);
      },
      (error) => {
        console.log(error);

      }
    );*/
  }
}
