import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Tutoaula';
  //@ViewChild('sidenav')
  loadLogo(){
    return "../assets/images/logo.png";
  }
  constructor(private router: Router, private route:ActivatedRoute){}
  ngOnInit(){
    
  }
  onLogout(){
    localStorage.setItem('token',null);
    this.router.navigate(['/login']);
  }
}
