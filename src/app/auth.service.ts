import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class AuthService {
    root_url = "http://api-tutoaula.sytes.net:1337/";
    constructor(private http: Http){}

    login(user,pass){
        let data = JSON.stringify ({
            "username" : user,
            "password" : pass
        });
        return this.http.post(this.root_url+'auth/login',data);
    }
    register(user,email,pass,rol){
        let data = JSON.stringify({
            "username":user,
            "password":pass,
            "email":email,
            "tipo":rol
        });
        return this.http.post(this.root_url+'auth/register',data);
    }
    getExcercise(){
        if(localStorage.getItem('token')!=null){
            const token = localStorage.getItem('token');
            //alert(token);
            let headers = new Headers();
            headers.append('Authorization', localStorage.getItem('token'));
            return this.http.get(this.root_url+'Ejercicio/');
        }else{
            console.log('error');
        }
        
    }
}