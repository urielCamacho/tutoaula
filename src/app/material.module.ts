import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatCheckboxModule, MatInputModule,
        MatSidenavModule, MatToolbarModule,
        MatIconModule, MatListModule, MatCardModule,MatProgressBarModule, MatGridListModule } from '@angular/material';
import {MatRadioModule} from '@angular/material/radio';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSnackBarModule} from '@angular/material/snack-bar';
@NgModule({
    imports : [MatButtonModule,MatCheckboxModule, MatInputModule,MatRadioModule,
        MatSidenavModule, MatToolbarModule,
    MatIconModule, MatListModule, MatCardModule, MatProgressBarModule,MatGridListModule,
    MatExpansionModule, MatSnackBarModule],
    exports : [MatButtonModule,MatCheckboxModule, MatInputModule, MatRadioModule,
        MatSidenavModule, MatToolbarModule,
    MatIconModule, MatListModule, MatCardModule, MatProgressBarModule, MatGridListModule,
    MatExpansionModule, MatSnackBarModule],
})
export class MaterialModule{}