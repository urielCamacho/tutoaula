import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Response } from '@angular/http';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
interface UserResponse {
  rol: string;
  username: string;
  email: string;
  createdAt: string;
  updatedAt: string;
  token: string;
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username : string = "";
  token : string = "";
  public user : UserResponse[];
  invalidation = false;
  constructor(private authService : AuthService, private router: Router) { }

  ngOnInit() {
  }
  loadLogo(){
    return "../../assets/images/logo.png";
  }
  loadFont2(){
    return "../../assets/images/font-2.jpeg";
  }
  check_user(){
    return this.username!=''?true:false;
  }
  onSubmit(form : NgForm){
    console.log(form);
    //alert("hola "+form.form.controls.username.value+form.form.controls.password.value);
    if(!form.invalid){
      this.authService.login(form.form.controls.username.value,form.form.controls.password.value)
      .subscribe(
        (data:Response) => {
          const json = data.json();
          //console.log(json);
          //alert(json.data.token);
          localStorage.setItem("token",json.data.token);
          //alert(localStorage.getItem("token"));
          this.router.navigate(['']);
        },
        (error:Response) => {
          const json = error.json();
          form.form.reset;
          this.invalidation = true;
        }
      );
    }
      
    
  }
}
