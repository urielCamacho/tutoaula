import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpModule } from '@angular/http';

import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { SelectModuleComponent } from './select-module/select-module.component';
import { TestComponent } from './test/test.component';
import { ProfileComponent } from './profile/profile.component';
import { ExcercisesComponent } from './excercises/excercises.component';
import { AuthService } from './auth.service';
//Definiendo nuestras rutas
const appRoutes : Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'registro', component: RegisterComponent},
  {path: 'perfil', component: ProfileComponent},
  {path: '', component: SelectModuleComponent},
  { path: 'ejercicios', component: ExcercisesComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    SelectModuleComponent,
    TestComponent,
    ProfileComponent,
    ExcercisesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    MaterialModule, 
    FormsModule, 
    FlexLayoutModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
